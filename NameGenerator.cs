using System;
using System.Collections;

namespace ConsoleApplication {
    class NameGenerator {
        static Random rnd = new Random();
        static void Main(string[] args) {
            string[] prefixes = loadFile("prefixes.txt");
            string[] suffixes = loadFile("suffixes.txt");
            string[] weapons = loadFile("weapons.txt");
            
            string prefix = "";
            string suffix = "";
            foreach (string weapon in weapons) {
                prefix = getRandom(prefixes);
                suffix = getRandom(suffixes);
                Console.WriteLine("{0} {1} of {2} ", prefix, weapon, suffix);
            }

            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        static string[] loadFile(string name) {
            string path = string.Format("./{0}", name);
            return System.IO.File.ReadAllLines(@path);
        }

        static string getRandom(string[] someArray) {
            return someArray[rnd.Next(someArray.Length)];
        }
    }
}

